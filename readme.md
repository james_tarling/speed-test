# speed test

Based on [automated-chrome-profiling](https://github.com/paulirish/automated-chrome-profiling) and [lighthouse](https://github.com/GoogleChrome/lighthouse/)

## Usage

**Run Chrome with an open debugging port**

```sh
# linux
google-chrome --remote-debugging-port=9222 --user-data-dir=$TMPDIR/chrome-profiling --no-default-browser-check

# mac
/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --remote-debugging-port=9222 --user-data-dir=$TMPDIR/chrome-profiling --no-default-browser-check
```
Navigate off the start page to example.com or something.

Make sure DevTools isn't open, and that any dodgy SSL certificates are authorised before starting your tests.

**Run the node script**

```
node index.js --repeat=NUM --config=FILE.json --description='Test name' [--throttle]
```