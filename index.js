const promiseChain = require('./utils/promise-chain');
const getKeyTimelineEvents = require('./get-key-timeline-events');
const minimist = require('minimist');
const fs = require('fs');
const promisify = require('util-promisify');


/**
 * Get and check arguments
 */

const argv = minimist(process.argv.slice(2));

let goodParams = true;

const repeat = parseInt(argv.repeat);
goodParams = goodParams && !isNaN(repeat);

const configFile = argv.config;
goodParams = goodParams && configFile && configFile.endsWith('.json');

const throttle = argv.throttle != undefined;

let description = argv.description || "Test"

if (!goodParams)
{
    console.log(
`Usage:
node index.js --repeat=NUM --config=FILE.json --description='Test name' [--throttle]
`);
    process.exit();
}

promisify(fs.readFile)(configFile)
.then((text) => {

    const pages = JSON.parse(text);

    const tests = [];

    //loop through pages defined in config json file
    //collect data on each page for repeat times
    
    pages.forEach(pageData => {

        const allResults = [];

        for (let i = 0; i < repeat; i++) {
            promiseChain.add(() => {
                console.log(pageData.name, 'test #' + (i + 1));
                return getKeyTimelineEvents(pageData.url, throttle)
                .then(results => {
                    allResults.push(results);
                });
            });
        }

        tests.push({
            pageData,
            allResults
        });
    });

    promiseChain.add(() => {
        return Promise.resolve(tests);
    });

    return promiseChain.get();
}, err => {
    console.error('Error reading file');
    process.exit(1);
})
.then(tests => {
    const exampleResult = tests[0].allResults[0];
    const metricNames = Object.keys(exampleResult);
    
    const analysedTestData = aggregateTestMetrics(metricNames, tests);
    const csv = convertTestDataToCSV(metricNames, tests, analysedTestData);
    console.log(csv);
})
.catch(err => {
    console.error(err);
    process.exit(1);
})

function convertTestDataToCSV(metricNames, tests, groupedMetricsPerTest) {
    // make CSV header
    let headerNames = [
        'Test',
        'Repeated',
        'Page name',
        'URL'
    ];
    const exampleGroup = groupedMetricsPerTest[0];
    const groupNames = Object.keys(exampleGroup);
    groupNames.forEach(groupKey => {
        metricNames.forEach(metricName => {
            headerNames.push(groupKey + ':' + metricName);
        });
    });

    // make CSV values
    const commonValues = [
        description,
        repeat
    ];
    const rows = groupedMetricsPerTest.map((testGroup, i) => {
        const pageData = tests[i].pageData;
        let row = commonValues.concat([pageData.name, pageData.url]);
        groupNames.forEach(groupKey => {
            let group = testGroup[groupKey];
            metricNames.forEach(metricName => {
                row.push(Math.round(group[metricName]));
            });
        });
        return row.join(',');
    });

    return `${headerNames.join(',')}
${rows.join('\n')}`
}

function aggregateTestMetrics(metricNames, tests) {
    return tests.map(test => {

        /*
        Work out the min, max and average of each metric
        */
        const sum = {};
        const sumSq = {};
        const min = {};
        const max = {};
        const values = {};

        metricNames.forEach(metric => {
            sum[metric] = 0;
            sumSq[metric] = 0;
            min[metric] = Number.MAX_VALUE;
            max[metric] = 0;

            values[metric] = [];

            test.allResults.forEach(result => {
                const val = result[metric];
                sum[metric] += val;
                sumSq[metric] += (val * val);

                min[metric] = Math.min(min[metric], val);
                max[metric] = Math.max(max[metric], val);

                values[metric].push(val);
            });
        });

        const mean = {};
        const median = {};
        const correctedMean = {};
        const deviation = {};
        metricNames.forEach(metric => {
            const _mean = mean[metric] = sum[metric] / repeat;
            median[metric] = ((max[metric] - min[metric])/2) + min[metric];

            const variance = sumSq[metric] / repeat - (_mean * _mean);
            const sd = Math.sqrt(variance);
            const dev = 3 * sd;

            deviation[metric] = sd;
            
            const corrected = values[metric].filter(val => {
                return val > _mean - dev && val < _mean + dev
            });
            const correctedSum = corrected.reduce((prev, current) => {
                return prev + current;
            }, 0);
            correctedMean[metric] = correctedSum / corrected.length;
        });

        return {
            min,
            max,
            //mean,
            //median,
            correctedMean,
            //deviation
        }
    });
}