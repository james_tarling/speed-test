var lastPromise;

function add(callback) {
  if (lastPromise)
  {
    lastPromise = lastPromise.then(callback).catch((err)=>{
      //discard rest of promise chain
      console.error(err);
      lastPromise = null;
      return null;
    });
  } else {
    lastPromise = callback();
  }
}

function get() {
    return lastPromise;
}

module.exports = {
    add,
    get
}