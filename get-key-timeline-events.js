require('./utils/stable-sort');

const throttleSettings = require('./throttling');

var Chrome = require('chrome-remote-interface');

var TRACE_CATEGORIES = [
    "-*", 
    "devtools.timeline",
    "disabled-by-default-devtools.timeline", 
    "disabled-by-default-devtools.timeline.frame", 
    "blink.user_timing"
];

// taken from https://github.com/GoogleChrome/lighthouse/blob/7d12091a79a428bd0d82549090e0084df67334d6/lighthouse-core/gather/computed/trace-of-tab.js#L38
function filterEvents(traceEvents) {
    const keyEvents = traceEvents
      .filter(e => {
        return e.cat.includes('blink.user_timing') ||
          e.cat.includes('loading') ||
          e.cat.includes('devtools.timeline') ||
          e.name === 'TracingStartedInPage';
      })
      .stableSort((event0, event1) => event0.ts - event1.ts);

      // The first TracingStartedInPage in the trace is definitely our renderer thread of interest
    // Beware: the tracingStartedInPage event can appear slightly after a navigationStart
    const startedInPageEvt = keyEvents.find(e => e.name === 'TracingStartedInPage');
    // Filter to just events matching the frame ID for sanity
    const frameEvents = keyEvents.filter(e => e.args.frame === startedInPageEvt.args.data.page);

    // Our navStart will be the last frame navigation in the trace
    const navigationStart = frameEvents.filter(e => e.name === 'navigationStart').pop();
    if (!navigationStart) throw new Error('navigationStart was not found in the trace');

    // Find our first paint of this frame
    const firstPaint = frameEvents.find(e => e.name === 'firstPaint' && e.ts > navigationStart.ts);

    // FCP will follow at/after the FP
    const firstContentfulPaint = frameEvents.find(
      e => e.name === 'firstContentfulPaint' && e.ts > navigationStart.ts
    );

    // fMP will follow at/after the FP
    let firstMeaningfulPaint = frameEvents.find(
      e => e.name === 'firstMeaningfulPaint' && e.ts > navigationStart.ts
    );

    // If there was no firstMeaningfulPaint event found in the trace, the network idle detection
    // may have not been triggered before Lighthouse finished tracing.
    // In this case, we'll use the last firstMeaningfulPaintCandidate we can find.
    // However, if no candidates were found (a bogus trace, likely), we fail.
    if (!firstMeaningfulPaint) {
      const fmpCand = 'firstMeaningfulPaintCandidate';
      console.log('trace-of-tab', `No firstMeaningfulPaint found, falling back to last ${fmpCand}`);
      const lastCandidate = frameEvents.filter(e => e.name === fmpCand).pop();
      if (!lastCandidate) {
        console.log('trace-of-tab', 'No `firstMeaningfulPaintCandidate` events found in trace');
      }
      firstMeaningfulPaint = lastCandidate;
    }

    const onLoad = frameEvents.find(e => e.name === 'loadEventEnd' && e.ts > navigationStart.ts);
    const domContentLoaded = frameEvents.find(
      e => e.name === 'domContentLoadedEventEnd' && e.ts > navigationStart.ts
    );

    const metrics = {
      firstPaint,
      firstContentfulPaint,
      firstMeaningfulPaint,
      domContentLoaded,
      onLoad
    };

    const timings = {};
    const timestamps = {};

    Object.keys(metrics).forEach(metric => {
      timestamps[metric] = metrics[metric] && metrics[metric].ts;
      timings[metric] = (timestamps[metric] - navigationStart.ts) / 1000;
    });

    return timings;
}

function getKeyTimelineEvents(url, throttle) {

    if (!url) return Promise.reject('url must be defined');

    return new Promise((resolve, reject) => {
        Chrome(chrome => {
            resolve(chrome);
        })
        .on('error', function (e) {
            reject('Cannot connect to Chrome');
        })
    })
    .then(chrome => {
        var rawEvents = [];
        var frameId;

        return chrome.HeapProfiler.collectGarbage()
        .then(() => {
            return chrome.Network.enable();
        })
        .then(() => {
            return chrome.Network.setCacheDisabled({cacheDisabled:true});
        })
        .then(() => {
            return chrome.Page.enable()
        })
        .then(() => {
            return chrome.Network.emulateNetworkConditions(
                throttle 
                ? 
                throttleSettings.TYPICAL_MOBILE_THROTTLING_METRICS
                :
                throttleSettings.NO_THROTTLING_METRICS
            );
        })
        .then(() => {
            return chrome.Tracing.start({
                "categories": TRACE_CATEGORIES.join(','),
                "options": "sampling-frequency=10000"  // 1000 is default and too slow.
            });
        })
        .then(() => {
            chrome.Page.frameStoppedLoading(function (data) {
                if (data.frameId == frameId)
                {
                    /*
                    Lighhouse checks for 3 things - Page.loadEventFired, idle network, and idle CPU
                    https://github.com/GoogleChrome/lighthouse/blob/master/lighthouse-core/gather/driver.js#L529
                    The implementation has a lot of dependencies on other libraries.
                    setTimeout fudge seems to work, but will obviously slow each test
                    */
                    setTimeout(function() {
                        chrome.Tracing.end();
                    }, 750);
                }
            });
            chrome.Tracing.dataCollected(function (data) {
                var events = data.value;
                rawEvents = rawEvents.concat(events);
            });

            chrome.Page.navigate({url})
            .then(res => {
                frameId = res.frameId;
            });
            return new Promise((resolve, reject) => {
                chrome.Tracing.tracingComplete(() => {
                    chrome.close();
                    resolve(filterEvents(rawEvents));
                });
            });
        })
    })
}

module.exports = getKeyTimelineEvents;